## Run the project
1. Clone the git repository.
2. Copy the ```.env.example``` file and rename it to ```.env```
3. Run ```composer install```.
4. Run ```php artisan generate:key```
5. Run ```php artisan cache:clear```
6. Run ```php artisan config:clear```
7. Run ```php artisan serve``` ta da! the app is up and you can follow the link in your console.

## The Proxy Task
- All configurations exist in the .env file for production and in the .env.testing for testing.
- You can run anything under the testing environment by adding the flag ```--env=testing``` to any artisan command.
- Default values exist in the ```config/innosabi.php``` file in case .env was missing.
- You can hit the following endpoint directly to test it [GET]```/api/v1/innosabi-projects?include=prop1,prop2```

## The slider Task
- You can access it through ```/slider```

## The highlight Task
- You can access it through ```/search```

### Contact
- For further questions/information please get in touch: noureddinramadan@gmail.com
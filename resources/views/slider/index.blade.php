<!doctype html>
<html>

<head>
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
  
  <div class="row">
    <div class="col-md-8 offset-md-2">
      @foreach ($data as $project)
          
      @endforeach
      

      <div id="projectsCarousel" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
              @for($i = 0; $i < count($data); $i++)
                @if($i==0)
                  <li data-target="#projectsCarousel" data-slide-to="{{ $i }}" class="active"></li>
                @else
                <li data-target="#projectsCarousel" data-slide-to="{{ $i }}"></li>
                @endif
              @endfor
          </ol>
          <div class="carousel-inner">
            @for($i = 0; $i < count($data); $i++)
              @if($i==0)
                <div class="carousel-item active">
              @else
                <div class="carousel-item">
              @endif
                <img class="d-block w-100" src="{{ $data[$i]['photo'] }}" alt="{{ $data[$i]['name'] }}">
                <div class="carousel-caption d-none d-md-block">
                  <h5>{{ $data[$i]['name'] }}</h5>
                  <p>{{ $data[$i]['description'] }}</p>
                </div>
              </div>
            @endfor
            
          </div>
          <a class="carousel-control-prev" href="#projectsCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#projectsCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>

    </div>
  </div>
  
  {{-- bootstrap scripts --}}
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  {{-- end of bootstrap scripts --}}
</body>

</html>
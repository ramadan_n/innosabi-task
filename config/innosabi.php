<?php

return [

    /*
    |--------------------------------------------------------------------------
    | External API Credentials
    |--------------------------------------------------------------------------
    |
    |
    */

    'credentials' => [
        'username' => env('INNO_API_USERNAME', 'api@innosabi.com'),
        'password' => env('INNO_API_PASSWORD', '0thRuch0'),
    ],

    /*
    |--------------------------------------------------------------------------
    | External API Config
    |--------------------------------------------------------------------------
    | These values are the configuration to connect to the innosabi api.
    |
    */

    'api' => [
        'url' => env('INNO_API_URL', 'https://demo.innosabi.com/api'),
        'version' => env('INNO_API_VERSION', 'v2'),
    ],

];

<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\RequestHelper;
use Illuminate\Contracts\Config\Repository;

class ProjectsController extends Controller
{
    /**
     * @var Repository
     */
    private $config;

    public function __construct(Repository $config)
    {
        $this->config = $config;
    }

    public function __invoke(Request $request){

        $response = RequestHelper::call(
            $this->config->get('innosabi.api.url') . '/' . $this->config->get('innosabi.api.version') . '/project/filter',
            'GET',
            [
                'username' => $this->config->get('innosabi.credentials.username'),
                'password' => $this->config->get('innosabi.credentials.password')
            ]
        );

        if($response['code'] !== 200){

            abort(500, 'Couldn\'t connect to innosabi API.');
        }

        $response = $response['data'];

        if($request->has('include')){

            $attributes =  explode(',', $request->input('include'));
            $result = array();

            foreach($response['data'] as $entry){

                $row = array();

                foreach($attributes as $attribute){

                    if(array_key_exists($attribute, $entry)){

                        $row[$attribute] = $entry[$attribute];
                    }
                }

                array_push($result, $row);
            }

            return $result;
        }

        return $response['data'];
    }
}

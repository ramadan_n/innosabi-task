<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;

class SearchController extends Controller
{

    public function __invoke(){
        
        return view('search.index');
    }
}

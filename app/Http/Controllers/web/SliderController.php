<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Services\JsonService;
use Illuminate\Contracts\Config\Repository;

class SliderController extends Controller
{
    /**
     * @var Repository
     */
    private $config;
    /**
     * @var JsonService
     */
    private $service;

    public function __construct(Repository $config, JsonService $service)
    {
        $this->config = $config;
        $this->service = $service;
    }

    public function __invoke(){
        
        $projects = $this->service->getData('assets/data/projects.json');

        $data = array();
        foreach($projects['data'] as $project){

            $piece['photo'] = $this->getPhotoLink($project['image'], 600, 300);
            $piece['name'] = $project['name'];
            $piece['description'] = $project['description'];
            
            array_push($data, $piece);
        }

        return view('slider.index', ['data' => $data]);
    }

    private function getPhotoLink($hash, $width, $height){

        return $this->config->get('innosabi.api.url') . '/v4/media/' . $hash . '/thumbnail/width/' . $width .  '/height/' . $height . '/strategy/crop';
    }
}

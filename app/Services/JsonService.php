<?php

namespace App\Services;

class JsonService{

    public function getData($path){

        $json = file_get_contents($path);

        return json_decode($json, true);
    }
}
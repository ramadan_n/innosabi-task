<?php

namespace App\Helpers;

class RequestHelper
{
    public static function call($url, $httpVerb, $basic_auth)
    {
        $curl = \curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $httpVerb,
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Accept-Encoding: gzip, deflate",
                "Authorization: Basic " . base64_encode($basic_auth['username'] . ':' . $basic_auth['password']),
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Cookie: ignite=m80l34ctf7cnt7gk658c8pri85",
                "Host: demo.innosabi.com",
                "Postman-Token: 882eedba-3a50-4050-948d-3345b4054523,9084d819-9d74-4eb7-af6a-66f79dd6e420",
                "User-Agent: PostmanRuntime/7.19.0",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $error = curl_error($curl);
        $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if ($error)
        {
            return [
                'code' => $http_status,
                'data' => $error
            ];
        }
        else
        {
            $data = json_decode($response, true);

            return [
                'code' => $http_status,
                'data' => $data
            ];
        }
    }
}
